# mobile-app
Projet d'application mobile fait avec React Native

Le thème de l'application est libre, l'objectif est de créer une application avec React Native avec pour seule contrainte d'y intégrer une fonctionnalité propre à une utilisation mobile (utilisation d'un des capteurs, de la caméra, de la géolocalisation ou autre)

## Travail attendu
* Créer des maquettes mobiles de l'application
* Créer une API Rest avec Symfony (et pourquoi pas API Platform) et/ou utiliser une API publique s'il y a suffisamment à faire avec en front
* Réaliser l'interface avec React Native
